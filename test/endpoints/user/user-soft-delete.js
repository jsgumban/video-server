'use strict';

const request = require('supertest');
const app = require(`${process.cwd()}/app`);

describe('DELETE /user', () => {
   const user = {
      id: 1
   };

   const client = {
      id: 1
   };

   const authorization = `Bearer ${JwtService.generateToken(user, client)}`

   beforeEach(done => {
      migrate().then(() => {
         return seeds();
      }).then(() => {
         done();
      });
   });

   afterEach(done => {
      rollback().then(() => {
         done();
      });
   });

   it('returns a 200 when successful', done => {
      request(app)
         .delete('/v1/user')
         .set('Authorization', authorization)
         .expect(200)
         .end((err, res) => {
            done(err);
         });
   });
});
