'use strict';

const request = require('supertest');
const app = require(`${process.cwd()}/app`);

describe('PUT /room', () => {
   const user = {
      id: 1
   };

   const client = {
      id: 1
   };

   const authorization = `Bearer ${JwtService.generateToken(user, client)}`

   beforeEach(done => {
      migrate().then(() => {
         return seeds();
      }).then(() => {
         done();
      });
   });

   afterEach(done => {
      rollback().then(() => {
         done();
      });
   });

   it('returns a 200 when successful', done => {
      request(app)
         .put('/v1/room')
         .set('Authorization', authorization)
         .send({
            'guid': '123-123',
            'hosted_by': 2
         })
         .expect(200)
         .end((err, res) => {
            done(err);
         });
   });
});
