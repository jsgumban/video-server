'use strict';

const request = require('supertest');
const app = require(`${process.cwd()}/app`);

describe('GET /rooms/id/:guid', () => {
   const clientId = 'client_id';
   const clientSecret = 'client_secret';
   const buffer = Buffer.from(`${clientId}:${clientSecret}`);
   const authorization = `Basic ${buffer.toString('base64')}`;

   beforeEach(done => {
      migrate().then(() => {
         return seeds();
      }).then(() => {
         done();
      });
   });

   afterEach(done => {
      rollback().then(() => {
         done();
      });
   });

   it('returns a 200 when successful', done => {
      request(app)
         .get('/v1/rooms/username/jsgumban')
         .set('Authorization', authorization)
         .expect(200)
         .end((err, res) => {
            done(err);
         });
   });
});
