'use strict';
let Model = require('./');

module.exports = Model.extend({
  tableName: 'rooms',

  hasTimestamps: [
     'created_at',
     'updated_at',
  ],

   user_room: function () {
      return this.belongsTo(UserRoom, 'id', 'room')
   }
});
