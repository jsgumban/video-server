'use strict';
let Model = require('./');

module.exports = Model.extend({
   tableName: 'users_rooms',

   hasTimestamps: [
      'created_at',
      'updated_at',
   ],

   room: function () {
      this.belongsToMany(Room, 'room', 'id');
   }
});
