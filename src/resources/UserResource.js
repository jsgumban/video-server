const { Resource, ResourceCollection } = require('@senhung/http-resource');

class UserResource extends Resource {
   toJson(resource) {
      return {
         id: resource.id,
         username: resource.attributes.username,
         mobile_token: resource.attributes.mobile_token,
      }
   }
}

module.exports = {
   make:       (resource)  => new UserResource(resource),
   collection: (resources) => new ResourceCollection(resources, UserResource),
};
