const { Resource, ResourceCollection } = require('@senhung/http-resource');

class AuthResource extends Resource {
   toJson(resource) {
      const user = UserResource.make(resource.user).response().data;
      const client = resource.client;
      const token = JwtService.generateToken(user, client);

      return {
         user: user,
         token: token
      }
   }
}

module.exports = {
   make:       (resource)  => new AuthResource(resource),
   collection: (resources) => new ResourceCollection(resources, AuthResource),
};
