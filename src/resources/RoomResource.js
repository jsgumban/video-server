const { Resource, ResourceCollection } = require('@senhung/http-resource');

class RoomResource extends Resource {
   toJson(resource) {
      return {
         id: resource.attributes.id,
         guid: resource.attributes.guid,
         name: resource.attributes.name,
         capacity_limit: resource.attributes.capacity_limit,
         created_by: resource.attributes.created_by,
         hosted_by: resource.attributes.hosted_by
      }
   }
}

module.exports = {
   make:       (resource)  => new RoomResource(resource),
   collection: (resources) => new ResourceCollection(resources, RoomResource),
};
