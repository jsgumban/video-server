'use strict';
const { Validator } = require('node-input-validator');

const CreateRoomRequest = async (req, res, next) => {
   try {
      const v = new Validator(req.body, {
         name: 'required|string',
         capacity_limit: 'integer|max:5'
      });

      const matched = await v.check();
      if (!matched) {
         throw new ApiError({
            errors: v.errors,
            statusCode: 422,
            message: 'Invalid request'
         });
      }

      next();
   } catch (err) {
      res.status(err.status || 403).json( err);
   }
}

module.exports = CreateRoomRequest;
