'use strict';
const { Validator } = require('node-input-validator');

const UpdateUserRequest = async (req, res, next) => {
   try {
      const v = new Validator(req.body, {
         password: 'string',
         mobile_token: 'string',
      });

      const matched = await v.check();
      if (!matched) {
         throw new ApiError({
            errors: v.errors,
            statusCode: 422,
            message: 'Invalid request'
         });
      }

      const params = _.pick(req.body,[
         'password',
         'mobile_token'
      ]);

      if (params.password) {
         params.password = HashService.execute(params.password);
      }

      req.body = params;
      next();
   } catch (err) {
      res.status(err.status || 403).json( err);
   }
}

module.exports = UpdateUserRequest;
