'use strict';

const jwt = require('jsonwebtoken');
const jwtToken = process.env.JWT_TOKEN;
const jwtExp = process.env.JWT_TOKEN_EXP;

class JwtService {
   static generateToken(user, client) {
      const token = jwt.sign({ user, client }, jwtToken, { expiresIn: jwtExp });
      return token;
   }
};

module.exports = JwtService;
