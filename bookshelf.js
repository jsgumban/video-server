'use strict';

const config = require('./config/database');
const knex = require('knex')(config);

const bookshelf = require('bookshelf')(knex);
bookshelf.plugin(require('bookshelf-soft-delete'));
bookshelf.plugin(require('bookshelf-eloquent'));

module.exports = bookshelf;
