
**VIDEO SERVER APP**
-


- **npm run test** - run endpoint tests
- **npm run dev** - run the app
- **npm run db seed:run** - run the seed
- **num run db migrate:latest** - run db migrations



**ENDPOINTS**
- 
- POST /auth/register - register user
    - params: username, password, mobile_token(optional)
    - authentication: client id and client secret

- POST /auth/login - login user
    - params: username, password
    - authentication: client id and client secret

- PUT /user - update user's info
    - params: password, mobile_token

- DELETE /user - delete user

- GET /users/:username - get user's info by user's username
    - authentication: client id and client secret

- GET /users - get list of users
    - authentication: client id and client secret

- POST /room - create room and set current user as host
    - param: name, capacity_limit

- PUT /room - assign host
    - params: guid, hosted_by

- POST /room/join - join room
    - params: guid

- POST /room/leave - leave room
    - params: guid
    
- GET /rooms/id/:guid - get room info by guid
    - authentication: client id and client secret

- GET /rooms/username/:username - get all user's rooms by user's username
    - authentication: client id and client secret




