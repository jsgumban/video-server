exports.seed = function(knex, Promise) {
   return knex('clients').del().then(() => {
      return knex('clients').insert([
         { client_id: 'client_id', client_secret: 'client_secret', name: 'client_name' }
      ]);
   }).then(() => {
      return knex('users').del().then(() => {
         return knex('users').insert([
            {
               'username': 'jsgumban',
               'password': '$2b$10$uLycBbdRqlAPnNZG3H9kteXDD1vSG5uEhqgt1Q0qJrxTykP6DY2JW',  //password
               'mobile_token': 'random-token'
            },
            {
               'username': 'jsgumban1',
               'password': '$2b$10$uLycBbdRqlAPnNZG3H9kteXDD1vSG5uEhqgt1Q0qJrxTykP6DY2JW',  //password
               'mobile_token': 'random-token'
            }
         ]);
      });
   }).then(() => {
      return knex('rooms').del().then(() => {
         return knex('rooms').insert([{
            'name': 'jsgumban\'s room',
            'guid': '123-123',
            'created_by': 1,
            'hosted_by': 1
         }]);
      }).then((res) => {
         return knex('users_rooms').del().then(() => {
            return knex('users_rooms').insert([{
               'room': 1,
               'user': 1
            }]);
         })
      })
   })
};
