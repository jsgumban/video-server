exports.up = function(knex) {
   return knex.schema.createTable('rooms', table => {
      table.increments('id').unique().notNullable().primary();
      table.text('guid').notNullable().unique();
      table.string('name').notNullable();
      table.integer('capacity_limit').notNullable().defaultTo(5);
      table.integer('created_by').references('users.id').notNullable();
      table.integer('hosted_by').references('users.id').notNullable();
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
   });
};

exports.down = function(knex) {
   return knex.schema.dropTableIfExists('rooms');
};
