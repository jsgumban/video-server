exports.up = function(knex) {
   return knex.schema.createTable('users_rooms', table => {
      table.increments('id').unique().notNullable().primary();
      table.integer('room').references('rooms.id').notNullable();
      table.integer('user').references('users.id').notNullable();
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
   });
};

exports.down = function(knex) {
   return knex.schema.dropTableIfExists('users_rooms');
};
