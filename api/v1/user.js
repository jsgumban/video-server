const router = require('express').Router();

// update current user info
router.put('/', UpdateUserRequest, async (req, res) => {
   try {
      let user = req.user;

      user = user.set(req.body);
      user = await user.save();

      const response = UserResource.make(user).response();
      res.status(200).json(response);
   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});

// soft delete current user
router.delete('/', async (req, res) => {
   try {
      let user = req.user;
      user = user.set({
         'deleted_at': new Date()
      });

      user = await user.save();
      const response = UserResource.make(user).response();
      res.status(200).json(response);
   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});


module.exports = router;
