const router = require('express').Router();
const uuid = require("uuid");

// user joins the room
router.post('/join', JoinRoomRequest, async (req, res) => {
   try {
      const user = req.user;
      const guid = req.body.guid;

      // validate if the room exists
      const room = await Room.where({ 'guid': guid }).fetch().catch((err) => {
         throw new ApiError({
            message: 'Invalid room id'
         });
      });

      // check if user is already in the room
      await UserRoom.where({ user: user.id , room: room.id }).fetch().catch(async (err) => {
         // count number of users in the room
         const usersCount = await UserRoom.where({ room: room.id }).count();

         // if users is on max, user's not allowed to join
         if (usersCount <= room.get('capacity_limit')) {
            // add current user to the room
            await new UserRoom({
               'user': user.id,
               'room': room.id,
            }).save().catch(err => {
               throw new ApiError({
                  message: err.detail ?? err.message,
               });
            });
         } else {
            throw new ApiError({
               message: 'Max limit reached for new users'
            });
         }
      });

      res.status(200).json(room);
   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});

// user leaves the room
router.post('/leave', LeaveRoomRequest, async (req, res) => {
   try {
      const user = req.user;
      const guid = req.body.guid;

      // validate if the room exists
      const room = await Room.where({ 'guid': guid }).fetch().catch((err) => {
         throw new ApiError({
            message: 'Invalid room id'
         });
      });

      if (room.get('hosted_by') == user.id) {
         // remove room and all members belong to the room
         // if the host leave the room
         await UserRoom.where({ room: room.id }).destroy();
         await room.destroy();
      } else {
         // remove user in the room
         await UserRoom.where({ room: room.id, user: user.id }).destroy();
      }


      res.status(204).json();
   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});

// create room and assign to current user
router.post('/', CreateRoomRequest, async (req, res) => {
   try {
      // create room
      const room = await new Room({
         'guid': uuid.v1(),
         'name': req.body.name,
         'hosted_by': req.user.id,
         'created_by': req.user.id,
         'capacity_limit': req.body.capacity_limit
      }).save().catch(err => {
         throw new ApiError({
            message: err.detail ?? err.message,
         });
      });

      // create room for user
      await new UserRoom({
         'user': req.user.id,
         'room': room.id,
      }).save().catch(err => {
         throw new ApiError({
            message: err.detail ?? err.message,
         });
      });

      const response = RoomResource.make(room).response();
      res.status(200).json(response);
   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});

// assign host
router.put('/', AssignHostRequest, async (req, res) => {
   try {
      const user = req.user;
      const guid = req.body.guid;
      const hostedBy = req.body.hosted_by;

      // validate if the current user is the room host
      const room = await Room.where({ 'guid': guid, 'hosted_by': user.id }).fetch().catch((err) => {
         console.log('err', err);
         throw new ApiError({
            message: 'You are not allowed to assign a new host'
         });
      });

      // add host to the room if not a member yet
      await UserRoom.where({ user: hostedBy , room: room.id }).fetch().catch(async (err) => {
         // count number of users in room
         const usersCount = await UserRoom.where({ room: room.id }).count();
         console.log('usersCount', usersCount);

         if (usersCount <= room.get('capacity_limit')) {
            // add new host to the room
            await new UserRoom({
               'user': hostedBy,
               'room': room.id,
            }).save().catch(err => {
               console.log('-err', err);
               throw new ApiError({
                  message: err.detail ?? err.message,
               });
            });
         } else {
            throw new ApiError({
               message: 'Max limit reached for new users'
            });
         }
      });

      // update room's new host
      await room.set({
         'hosted_by': hostedBy
      }).save().catch((err) => {
         throw new ApiError({
            message: 'Invalid new host id'
         });
      });


      const response = RoomResource.make(room).response();
      res.status(200).json(response);

   } catch (err) {
      console.log('err', err);
      res.status(err.status || 403).json(err);
   }
});

module.exports = router;
