const router = require('express').Router();
const uuid = require("uuid");

// room info given room id
router.get('/id/:guid', async (req, res) => {
   try {
      const guid = req.params.guid;
      const rooms = await Room.where({ guid: guid }).fetch().catch(() => {
         throw new ApiError({
            message: 'Invalid guid'
         });
      });

      const response = RoomResource.make(rooms).response();
      res.status(200).json(response);
   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});

// list of rooms given username
router.get('/username/:username', async (req, res) => {
   try {
      const username = req.params.username;

      const user = await User.where({ username: username }).fetch().catch(() => {
         throw new ApiError({
            message: 'User does not exist'
         });
      });

      const rooms = await Room.whereHas('user_room', (query) => {
         query.where({ user: user.id })
      }).get();


      const response = RoomResource.collection(rooms).response();
      res.status(200).json(response);

   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});

module.exports = router;
