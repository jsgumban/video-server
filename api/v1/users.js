const router = require('express').Router();

// get user info by username
router.get('/:username', async (req, res) => {
   try {
      const username = req.params.username;

      const user = await User.where('username', username).fetch().catch(() => {
         throw new ApiError({
            statusCode: 404,
            message: 'User not found'
         });
      });
      const response = UserResource.make(user).response();
      res.status(200).json(response);
   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});


// get list of users
router.get('/', async (req, res) => {
   try {
      const query = req.query;
      const limit = query.limit || 10; // per-page
      const offset = query.offset ? parseInt(query.offset) - 1 : 10; // page

      const users = await User.collection().query({limit, offset}).fetch();
      const response = UserResource.collection(users).response();
      res.status(200).json(response);
   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});


module.exports = router;
