'use strict';
const auth = require('basic-auth');


module.exports = async (req, res, next) => {
   try {
      const creds = auth(req);
      if (!creds.name || !creds.pass) {
         // no basic auth passed
         throw new ApiError({
            statusCode: 403,
            message: 'Credentials are required'
         });
      }

      // validate username and password
      const client = await Client.where({ client_id: creds.name, client_secret: creds.pass }).fetch().catch(() => {
         // invalid basic auth passed
         throw new ApiError({
            statusCode: 401,
            message: 'Invalid credentials'
         });
      });

      req.client = client.get('client_id');
      next();
   } catch (err) {
      res.status(err.status || 403).json( err);
   }
};
