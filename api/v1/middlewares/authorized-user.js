'use strict';
const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
   try {
      const authHeader = req.headers['authorization'];
      const token = authHeader && authHeader.split(' ')[1];

      // no access token passed
      if (token == null) {
         throw new ApiError({
            statusCode: 403,
            message: 'Token is required'
         });
      }

      // veify access token
     await jwt.verify(token, process.env.JWT_TOKEN, async (err, data) => {
         // invalid token
         if (err) {
            throw new ApiError({
               statusCode: 401,
               message: 'Invalid token'
            });
         }

         const user = await User.where('id', data.user.id).fetch();
         req.user = user;
         req.client = data.client;
         next();
      });
   } catch (err) {
      res.status(err.status || 403).json( err);
   }
};
