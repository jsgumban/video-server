const router = require('express').Router();

const authorizedUser = require('./middlewares/authorized-user');
const authorizedClient = require('./middlewares/authorized-client');

const auth = require('./auth');
const user = require('./user');
const users = require('./users');
const room = require('./room');
const rooms = require('./rooms');

router.use('/auth', authorizedClient, auth);
router.use('/user', authorizedUser, user);
router.use('/users', authorizedClient, users);
router.use('/room', authorizedUser, room);
router.use('/rooms', authorizedClient, rooms);

module.exports = router;
