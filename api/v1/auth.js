const router = require('express').Router();


// register new user
router.post('/register', RegisterUserRequest, async (req, res) => {
   try {
      const client = req.client;

      const user = await new User({
         'username': req.body.username,
         'password': req.body.password,
         'mobile_token': req.body.mobile_token
      }).save().catch((err) => {
         throw new ApiError({
            message: err.detail ?? err.message,
         });
      });

      const response = AuthResource.make({ user, client }).response();
      res.status(201).json(response);
   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});


// login user
router.post('/login', LoginUserRequest, async (req, res) => {
   try {
      const client = req.client;
      const username = req.body.username;
      const password = req.body.password;

      const user = await User.where({'username': username}).fetch().catch((err) => {
         throw new ApiError({
            statusCode: 401,
            message: 'Invalid username/password',
         });
      });

      const matched = HashService.compare(
         password,
         user.get('password'),
      );

      if (!matched) {
         throw new ApiError({
            statusCode: 401,
            message: 'Invalid username/password',
         });
      }

      const response = AuthResource.make({ user, client }).response();
      res.status(200).json(response);
   } catch (err) {
      res.status(err.status || 403).json(err);
   }
});

module.exports = router;
